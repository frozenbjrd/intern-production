﻿using Appfriend;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form1
{
    public partial class FriendRequest : Form
    {
        public FriendRequest()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
            //select slt = new select();
            //slt.Show();
        }

        public void Load_data()
        {
            var requestlist = Waiting2.requestList.ToList();
            requestlist.OrderBy(key => key.Value.score);
            ImageList Imagelist = new ImageList();
            int numberfr = requestlist.Count;

            for (int i = numberfr - 1; i >= 0; i--)
            {
                string image = requestlist[i].Value.img;
                WebRequest requestPic = WebRequest.Create(image);
                WebResponse responsePic = requestPic.GetResponse();
                Image bmp = Image.FromStream(responsePic.GetResponseStream());
                Imagelist.Images.Add(bmp);
                string name = requestlist[i].Value.name;
                string id = requestlist[i].Key;
                ListViewItem it = new ListViewItem();
                it.ImageIndex = numberfr - i - 1;
                it.SubItems[0].Text = name;
                it.SubItems[0].ForeColor = ColorTranslator.FromHtml("#365899");
                listView1.Items.Add(it);
                listView1.Update();
                Application.DoEvents();

            }
            listView1.View = View.List;
            listView1.GridLines = true;
            Imagelist.ImageSize = new Size(50, 50);
            listView1.SmallImageList = Imagelist;
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

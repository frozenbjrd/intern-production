﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Chrome;

namespace Appfriend
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
            //    listView1.SmallImageList = Imagelist;

        }
        public void Load_data()
        {
            int count = 0;
            ImageList Imagelist = new ImageList();
            int numberfr = Waiting.rank.Count;

            for (int i = numberfr - 1; i >= 0; i--)
            {
                string image = Waiting.rank.ElementAt(i).Value.img;
                WebRequest requestPic = WebRequest.Create(image);
                WebResponse responsePic = requestPic.GetResponse();
                Image bmp = Image.FromStream(responsePic.GetResponseStream());
                Imagelist.Images.Add(bmp);
                string name = Waiting.rank.ElementAt(i).Value.name;
                string id = Waiting.rank.ElementAt(i).Key;
                ListViewItem it = new ListViewItem();
                it.ImageIndex = numberfr - i - 1;
                it.SubItems[0].Text = name;
                it.SubItems[0].ForeColor = ColorTranslator.FromHtml("#365899");
                listView1.Items.Add(it);
                listView1.Update();
                Application.DoEvents();
                Waiting.tb.Add(numberfr - i - 1, id);

            }
            listView1.View = View.List;
            listView1.GridLines = true;
            listView2.View = View.List;
            listView2.GridLines = true;
            Imagelist.ImageSize = new Size(50, 50);
            listView1.SmallImageList = Imagelist;
            listView2.SmallImageList = Imagelist;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int key = listView1.SelectedItems[0].ImageIndex;
                string url = "http://www.facebook.com/" + Waiting.tb[key];
                Form1.driver.Navigate().GoToUrl(url);
            } catch
            {

            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int key = listView2.SelectedItems[0].ImageIndex;
                string url = "http://www.facebook.com/" + Waiting.tb[key];
                Form1.driver.Navigate().GoToUrl(url);
            } catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            while (listView1.CheckedItems.Count > 0)
            {
                ListViewItem it = listView1.CheckedItems[0];
                listView1.Items.Remove(it);
                listView2.Items.Add(it);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            while (listView2.CheckedItems.Count > 0)
            {
                ListViewItem it = listView2.CheckedItems[0];
                listView2.Items.Remove(it);
                listView1.Items.Add(it);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView2.Items.Count > 0)
            {
                while (listView2.Items.Count > 0)
                {
                    ListViewItem it = listView2.Items[0];
                    int name = it.ImageIndex;
                    Form1.driver.Url = "https://www.facebook.com/" + Waiting.tb[name];
                    IWebElement friend = Form1.driver.FindElement(By.XPath("//a[@data-floc='profile_button']"));
                    var test = friend.GetAttribute("innerHTML");
                    Actions action = new Actions(Form1.driver);
                    action.MoveToElement(friend).Perform();
                    Thread.Sleep(2000);
                    IWebElement unfriend = Form1.driver.FindElement(By.XPath("//a[contains(@ajaxify,'/ajax/profile/removefriendconfirm.php?')]"));
                    unfriend.Click();
                    listView2.Items.Remove(it);
                }
                Success success = new Success();
                success.Show();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form1 fr1 = new Form1();
            fr1.Show();
            Hide();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Hide();
            Waiting2 wt2 = new Waiting2();
            wt2.Show();
            wt2.GetFriendRequest();
            wt2.LoadRequest();
        }
    }

    public class User
    {
        public string name { get; set; }
        public string img { get; set; }
        public double score { get; set; }


    }
}

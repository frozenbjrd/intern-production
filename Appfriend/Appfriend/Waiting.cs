﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appfriend
{
    public partial class Waiting : Form
    {
        public Waiting()
        {
            InitializeComponent();

        }
        public static Hashtable tb = new Hashtable();
        public static Dictionary<string, User> rank = new Dictionary<string, User>();
        public  List<string> check = new List<string>();

        private void Waiting_Load(object sender, EventArgs e)
        {
            
            progressBar1.Increment(5);
            progressBar1.Update();
            Application.DoEvents();


        }
        public void Load_datafr()
        {
            IWebElement user = Form1.driver.FindElement(By.ClassName("_2s25"));
            string[] tm = user.FindElement(By.XPath("img")).GetAttribute("id").Split('_');
            //IWebElement user2 = Form1.driver.FindElement(By.ClassName("_2s25"));
            //user2.Click();
            //Thread.Sleep(1000);
            //IWebElement friend = Form1.driver.FindElement(By.XPath("//a[@data-tab-key='friends']"));
            //friend.Click();
            //Thread.Sleep(1000);
            Form1.driver.Url = "http://www.facebook.com/" + tm[3] + "/friends";
            List<string> link_friend = new List<string>();
            IJavaScriptExecutor js = (IJavaScriptExecutor)Form1.driver;
            
            int count = Convert.ToInt16(Form1.driver.FindElement(By.ClassName("_3s-")).FindElement(By.ClassName("_3d0")).GetAttribute("innerHTML"));
            int up = count / 26;
            progressBar1.Value++;
            progressBar1.Update();
            Application.DoEvents();
            while (count > 0)
            {
                js.ExecuteScript("javascript:window.scrollBy(0,document.body.scrollHeight)");
                if (progressBar1.Value <= 60)
                {
                    progressBar1.Value++;
                    progressBar1.Update();
                    Application.DoEvents();
                }
                count -= 26;
                Thread.Sleep(2000);
            }

            IList<IWebElement> list_friend = Form1.driver.FindElements(By.XPath("//ul[@data-pnref='friends']//li"));
            foreach (IWebElement iwe in list_friend)
            {
                try
                {

                    IWebElement a = iwe.FindElement(By.XPath("div/a/img"));
                    IWebElement c = iwe.FindElement(By.ClassName("fcb")).FindElement(By.XPath("a"));
                    var json = JObject.Parse(c.GetAttribute("data-gt"));
                    string id = (string)json["engagement"]["eng_tid"];
                    User x = new User();
                    x.name = c.GetAttribute("innerHTML");
                    x.img = a.GetAttribute("src");
                    x.score = 0;
                    rank.Add(id, x);
                    check.Add(id);
                }
                catch
                {

                }
            }
            
            Form1.driver.Url = "http://www.facebook.com/ajax/typeahead/search/facebar/bootstrap/?viewer=" + tm[3] + "&__a=1";

            string tmp = Form1.driver.FindElement(By.TagName("pre")).GetAttribute("innerHTML");
            Form1.driver.Url = "http://www.facebook.com";
            progressBar1.Increment(20);
            progressBar1.Update();
            Application.DoEvents();
            string data = tmp.Replace("for (;;);", string.Empty);
            var jso = JObject.Parse(data);
            foreach (var json in jso["payload"]["entries"])
            {
                string id = (string)json["uid"];
                if (check.Contains(id))
                {
                    rank[id].score = Convert.ToDouble(json["grammar_costs"]["user"]);
                }
            }
            progressBar1.Increment(80);
            progressBar1.Update();
            Application.DoEvents();
            
            //Hide();
            Form2 fr2 = new Form2();
            fr2.Show();
            fr2.Load_data();
            Hide();
        }
        public void GetFriendList()
        {

           
        }


    }
}

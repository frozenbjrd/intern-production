﻿using Form1;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appfriend
{
    public partial class Waiting2 : Form
    {
        public Waiting2()
        {
            InitializeComponent();
        }
        public static Dictionary<string, User> requestList = new Dictionary<string, User>();
        private void progressBar1_Click_1(object sender, EventArgs e)
        {
      
        }

        private void Waiting_Load(object sender, EventArgs e)
        {
            progressBar1.Increment(5);
            progressBar1.Update();
            Application.DoEvents();
        }
        public void LoadRequest()
        {
            foreach (var us in requestList)
            {
                try
                {
                    Form1.driver.Url = "https://www.facebook.com/" + us.Key + "/friends_mutual";
                    Thread.Sleep(1000);
                    IJavaScriptExecutor js = (IJavaScriptExecutor)Form1.driver;
                    js.ExecuteScript("javascript:window.scrollBy(0,document.body.scrollHeight)");
                    Thread.Sleep(2000);
                    js.ExecuteScript("javascript:window.scrollBy(0,document.body.scrollHeight)");
                    Thread.Sleep(2000);
                    js.ExecuteScript("javascript:window.scrollBy(0,document.body.scrollHeight)");
                    Thread.Sleep(2000);
                    IList<IWebElement> fr = Form1.driver.FindElements(By.XPath("//div[@data-pnref='mutual']"));
                    foreach (IWebElement we in fr)
                    {
                        try
                        {
                            IWebElement c = we.FindElement(By.ClassName("fcb")).FindElement(By.XPath("a"));
                            var json = JObject.Parse(c.GetAttribute("data-gt"));
                            string newid = (string)json["engagement"]["eng_tid"];
                            us.Value.score += Waiting.rank[newid].score;
                        }
                        catch { }
                    }
                    if (progressBar1.Value <= 95)
                    {
                        progressBar1.Increment(1);
                        progressBar1.Update();
                        Application.DoEvents();
                    }
                    }
                catch { }
            }
            progressBar1.Increment(70);
            progressBar1.Update();
            Application.DoEvents();
            this.Hide();
            FriendRequest fqForm = new FriendRequest();
            fqForm.Show();
            fqForm.Load_data();
        }
        public void GetFriendRequest()
        {
            Form1.driver.Url = "https://www.facebook.com/friends/requests/";
            IWebElement number_of_request = Form1.driver.FindElement(By.ClassName("_34e"));
            int numberOfRequest = Int32.Parse(Regex.Match(number_of_request.GetAttribute("innerHTML"), @"\d+").Value);
            int maxPressTime;
            if (numberOfRequest % 50 == 0)
            {
                maxPressTime = numberOfRequest / 50;
            }
            else maxPressTime = (int)Math.Floor((double)numberOfRequest / 50);
            int j = 0;
            progressBar1.Increment(5);
            progressBar1.Update();
            Application.DoEvents();
            while (j < maxPressTime)
            {
                IWebElement seemorebutton = Form1.driver.FindElement(By.Id("FriendRequestMorePager"));
                seemorebutton.Click();
                j++;
                Thread.Sleep(2000);
            }
            progressBar1.Increment(15);
            progressBar1.Update();
            Application.DoEvents();
            IList<IWebElement> list_request = Form1.driver.FindElements(By.ClassName("friendRequestItem"));
            progressBar1.Increment(5);
            progressBar1.Update();
            Application.DoEvents();
            foreach (var iw in list_request)
            {
                try
                {
                    User newRequest = new User();
                    string newId = iw.GetAttribute("data-id");
                    IWebElement photo = iw.FindElement(By.ClassName("img"));
                    newRequest.img = photo.GetAttribute("src");
                    IWebElement name = iw.FindElement(By.ClassName("_6-_")).FindElement(By.XPath("a"));
                    newRequest.name = name.GetAttribute("innerHTML");
                    newRequest.score = 0;
                    requestList.Add(newId, newRequest);
                }
                catch
                {

                }
            }
            progressBar1.Increment(10);
            progressBar1.Update();
            Application.DoEvents();
        }
        private void label1_Click(object sender, EventArgs e)
        {
  
        }
    }
}
